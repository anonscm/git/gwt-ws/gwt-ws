/*
 * GWT WebService,
 * GWT WebService Implementation
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'GWT WebService'
 * Signature of Elmar Geese, 25 August 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.gwt.ws.client.types;

import java.util.List;

import com.google.gwt.xml.client.Element;
import com.google.gwt.xml.client.Node;

/**
 * Deserializes a given list of Entry elements recursively to XML.
 * This acts as a default serializer.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public class MapSerializer implements Serializer
{
    public Node serialize(Node rootNode, Object object)
    {
        if (object instanceof de.tarent.gwt.ws.client.soap.Node && ((de.tarent.gwt.ws.client.soap.Node)object).getType()==de.tarent.gwt.ws.client.soap.Node.OBJECT_NODE)
        {
            // is an SOAP implementation object node
            object = ((de.tarent.gwt.ws.client.soap.Node)object).getValue();
        }
        
        if (object instanceof List)
        {
            List elementList = (List)object;
            
            Element thisListElement = rootNode.getOwnerDocument().createElement("List");
            
            for (int i=0;i<elementList.size();i++)
            {
                Entry thisEntry = (Entry)elementList.get(i);
                thisListElement.appendChild(serialize(thisListElement, thisEntry));
            }            
            
            return thisListElement;
        }
        else if (object instanceof Entry)
        {
            Entry entry = (Entry)object;
            Element thisElement = rootNode.getOwnerDocument().createElement(entry.getKey());
            thisElement.appendChild(serialize(thisElement, entry.getValue()));
            return thisElement;
        }
        else
        {
            return SerializerUtils.baselevelSerialize(rootNode, object);
        }
    }
}
