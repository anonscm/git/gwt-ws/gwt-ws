/**
 * 
 */
package de.tarent.gwt.ws.octopus.client;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public interface OctopusResponseListener {

	public void responseReceived(OctopusGWTResult result);
	
	public void errorReceived(Throwable exception);
}
