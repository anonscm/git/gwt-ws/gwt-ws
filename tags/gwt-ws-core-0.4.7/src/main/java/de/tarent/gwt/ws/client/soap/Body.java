/*
 * GWT WebService,
 * GWT WebService Implementation
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'GWT WebService'
 * Signature of Elmar Geese, 25 August 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 21.12.2007
 */

package de.tarent.gwt.ws.client.soap;

import java.util.List;

/**
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 * 
 */
public class Body extends Element
{

	public static final String namespace_SOAP11 = Envelope.namespace_SOAP11;
    public static final String namespace_SOAP12 = Envelope.namespace_SOAP12;
    public static final String name = "Body";

    private Fault fault = null;
    
    public Body() throws NodeException
    {
        super();
    }
    
    /**
	 * @see de.tarent.gwt.ws.client.soap.Element#addAttribute(de.tarent.gwt.ws.client.soap.Attribute)
	 */
	@Override
	public void addAttribute(Attribute attribute) throws SoapException {

		 if (!attribute.isQualified())
	            throw new SoapException("Top level attributes in SOAP bodies must be namespace-qualified: " + attribute.getName());
	        
	     getChilds().add(attribute);
	}

    public Fault getFault()
    {
        return fault;
    }

    public void setFault(Fault fault) throws SoapException
    {
        if (!getChilds().isEmpty())
            throw new SoapException("This SOAP body already contains child elements. Can't add fault.");

        this.fault = fault;
    }
    
    public boolean isFaulted()
    {
        return fault!=null;
    }
    
    /**
	 * @see de.tarent.gwt.ws.client.soap.Node#addChild(int, de.tarent.gwt.ws.client.soap.Node)
	 */
	@Override
	public void addChild(int index, Node child) throws NodeException {
		
		if (isFaulted())
            throw new NodeException("This SOAP body contains a fault element. Can't add other elements.");

        super.addChild(index, child);
	}

	public Object getDeserializedData()
    {
        // returns the first object node to be found at toplevel.
        // this only works for rpc style
        List<Node> childs = getChilds();
        
        for (int i=0; i < childs.size(); i++)
            if (childs.get(i).getType() == Node.OBJECT_NODE)
                return childs.get(i).getValue();

        return null;
    }
}
