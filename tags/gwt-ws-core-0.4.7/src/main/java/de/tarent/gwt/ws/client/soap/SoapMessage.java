/*
 * GWT WebService,
 * GWT WebService Implementation
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'GWT WebService'
 * Signature of Elmar Geese, 25 August 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.gwt.ws.client.soap;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Element;
import com.google.gwt.xml.client.NamedNodeMap;
import com.google.gwt.xml.client.Node;
import com.google.gwt.xml.client.NodeList;
import com.google.gwt.xml.client.XMLParser;

import de.tarent.gwt.ws.client.types.Deserializer;
import de.tarent.gwt.ws.client.types.Serializer;
import de.tarent.gwt.ws.client.types.SerializerUtils;

/**
 * Represents a SOAP message.
 * 
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public class SoapMessage
{
    public static final String XSD_NAMESPACE = "http://www.w3.org/2001/XMLSchema";
    public static final String XSI_NAMESPACE = "http://www.w3.org/2001/XMLSchema-instance";
    
    public static final int CONTEXT_ENVELOPE = 100;
    public static final int CONTEXT_HEADER = 200;
    public static final int CONTEXT_BODY = 300;
    
    public static final int SOAP_VERSION_11 = 11;
    public static final int SOAP_VERSION_12 = 12;
    
    private int soapVersion = -1;
    private Envelope envelope = null;    
    
    public SoapMessage(int soapVersion) throws NodeException
    {
        // constructs an empty SOAP message
        super();
        this.soapVersion = soapVersion;
        
        envelope = new Envelope();
        envelope.createHeader();
        envelope.createBody();
    }
    
    public SoapMessage(Element documentElement) throws NodeException
    {
        super();
        soapVersion = getSoapVersion(documentElement);
        
        checkQNameOrFail(documentElement, getSpecificationNamespaceURI(CONTEXT_ENVELOPE), Envelope.name);
        envelope = parseEnvelope(documentElement);
    }

    private String getSpecificationNamespaceURI(int context) throws SoapException
    {
        switch (context + soapVersion)
        {
            case 111: return Envelope.namespace_SOAP11;
            case 112: return Envelope.namespace_SOAP12; 
            case 211: return Header.namespace_SOAP11;
            case 212: return Header.namespace_SOAP12;
            case 311: return Body.namespace_SOAP11;
            case 312: return Body.namespace_SOAP12;
            default: throw new SoapException("Unknown context " + context 
                    + ". Expected either ENVELOPE, HEADER or BODY.");
        }
    }
    
    private int getSoapVersion(Element documentElement) throws SoapException
    {
        if (Envelope.namespace_SOAP11.equals(documentElement.getNamespaceURI()))
                return SoapMessage.SOAP_VERSION_11;
        else if (Envelope.namespace_SOAP12.equals(documentElement.getNamespaceURI()))
                return SoapMessage.SOAP_VERSION_12;
        else
            throw new SoapException("Encountered unknown envelope namespace " 
                    + documentElement.getNamespaceURI() 
                    + ". Expected either " + Envelope.namespace_SOAP11 
                    + " or " + Envelope.namespace_SOAP12);      
    }
    
    public Header getHeader()
    {
        return envelope.getHeader();
    }

    public Body getBody()
    {
        return envelope.getBody();
    }
    
    public Document toXml()
    {
        Document xml = XMLParser.createDocument();
        xml.appendChild(xml.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\""));
        
        // serialize envelope
        xml.appendChild(serializeEnvelope(xml));
                
        return xml;
    }
    
    private Node serializeEnvelope(Document xml)
    {
        Element envelope = xml.createElement("soap:" + Envelope.name);
        envelope.setAttribute("xmlns:soap", Envelope.namespace_SOAP12);
        
        // Define xsd and xsi
        envelope.setAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
        envelope.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");

        // add header
        envelope.appendChild(serializeHeader(xml));

        // add body
        envelope.appendChild(serializeBody(xml));
        
        return envelope;
    }

    private Node serializeHeader(Document xml)
    {
        Element header = xml.createElement("soap:" + Header.name);

        // Header element is optional
        if (getHeader().getChilds()!=null)
            for (int i=0; i<getBody().getChilds().size();i++)
            {
                Node newChild = serializeChildElements(xml, header, (de.tarent.gwt.ws.client.soap.Node)getHeader().getChilds().get(i));
                if (newChild!=null)
                    header.appendChild(newChild);
            }
        
        return header;
    }

    private Node serializeBody(Document xml)
    {
        Element body = xml.createElement("soap:" + Body.name);

        for (int i=0; i<getBody().getChilds().size();i++)
        {
            Node newChild = serializeChildElements(xml, body, (de.tarent.gwt.ws.client.soap.Node)getBody().getChilds().get(i));
            if (newChild!=null)
                body.appendChild(newChild);
        }
        
        return body;
    }
    
    private Node serializeChildElements(Document xml, Element parent, de.tarent.gwt.ws.client.soap.Node source)
    {
        if (source!=null)
        {
            if (source.getType()==de.tarent.gwt.ws.client.soap.Node.TEXT_NODE)
                return xml.createTextNode((String)source.getValue());
            else if (source.getType()==de.tarent.gwt.ws.client.soap.Node.OBJECT_NODE)
            {
                Serializer serializer = getSerializerForType(GWT.getTypeName(source.getValue()));                
                return serializer.serialize(parent, source);
            }
            else if (source.getType()==de.tarent.gwt.ws.client.soap.Node.ELEMENT_NODE)
            {
            	de.tarent.gwt.ws.client.soap.Element element = ((de.tarent.gwt.ws.client.soap.Element)source);
            	
            	String nodeName = (element.isQualified() ? "ns1:" : "") + element.getName();
            			
                Element parentElement = xml.createElement(nodeName);
                
                if(element.isQualified())
                	parentElement.setAttribute("xmlns:ns1", element.getNamespace());
                
                List<de.tarent.gwt.ws.client.soap.Node> childs = ((de.tarent.gwt.ws.client.soap.Element)source).getChilds();
                
                if(childs != null) {
                	
	                for (int i=0; i < childs.size(); i++) {
	                    Node n = serializeChildElements(xml, parentElement, childs.get(i));
	                    if (n != null)
	                    	parentElement.appendChild(n);
	                }
                }
                
                return parentElement;
            }
            else
                return null;
        }
        else 
            return null;
    }

    public String toString()
    {
        return this.toXml().toString();
    }
    
    private void checkQNameOrFail(Element element, String namespace, String name) throws SoapException
    {
        if (!checkElementQName(element, getSpecificationNamespaceURI(CONTEXT_ENVELOPE), Envelope.name))
            throw new SoapException("Encountered unknown element " 
                    + element.getNamespaceURI() + ":" 
                    + element.getNodeName() 
                    + ". Expected " + namespace + ":" + name);        
    }
    
    private boolean checkElementQName(Element element, String namespace, String name)
    {
        return namespace.equals(element.getNamespaceURI())
                || name.equals(SerializerUtils.mangleElementName(element.getNodeName()));
    }
    
    private Envelope parseEnvelope(Element envelopeElement) throws NodeException
    {
        Envelope envelope = new Envelope();
        
        Element headerElement = lookupElement(envelopeElement, getSpecificationNamespaceURI(CONTEXT_HEADER), Header.name);
        // Header element is optional
        if (headerElement!=null)
            envelope.setHeader(parseHeader(headerElement));

        Element bodyElement = lookupElement(envelopeElement, getSpecificationNamespaceURI(CONTEXT_BODY), Body.name);
        if (bodyElement==null)
            throw new SoapException("Can't find body element in SOAP message.");
        envelope.setBody(parseBody(bodyElement));
        
        return envelope;
    }
    
    private Header parseHeader(Element headerElement) throws SoapException, NodeException
    {
        checkQNameOrFail(headerElement, getSpecificationNamespaceURI(CONTEXT_HEADER), Header.name);
        
        Header header = new Header();
        NodeList childs = headerElement.getChildNodes();
        createChildElements(header, childs);
        
        return header;        
    }

    private Body parseBody(Element bodyElement) throws NodeException
    {
        checkQNameOrFail(bodyElement, getSpecificationNamespaceURI(CONTEXT_BODY), Body.name);
        
        Body body = new Body();
        NodeList childs = bodyElement.getChildNodes();
        createChildElements(body, childs);

        return body;
    }
    
    /**
     * Recursively traverses a given subtree, creating Element instances.
     * 
     * @param header
     * @param childs
     * @throws NodeException 
     */
    private de.tarent.gwt.ws.client.soap.Node createChildElements(de.tarent.gwt.ws.client.soap.Node root, NodeList childs) throws NodeException
    {
        if (childs!=null)
            for (int i=0; i<childs.getLength(); i++)
            {
                Node thisChild = childs.item(i);
                if (thisChild.getNodeType() == Node.TEXT_NODE)
                {
                    de.tarent.gwt.ws.client.soap.Node stringNode = new de.tarent.gwt.ws.client.soap.Node();
                    stringNode.setType(de.tarent.gwt.ws.client.soap.Node.TEXT_NODE);
                    stringNode.setValue(thisChild.getNodeValue());
                    root.addChild(stringNode);
                }
                else if (thisChild.getNodeType() == Node.ELEMENT_NODE)
                {
                     de.tarent.gwt.ws.client.soap.Node generatedChild = new de.tarent.gwt.ws.client.soap.Element(
                            new QName(thisChild.getNamespaceURI(), thisChild.getNodeName()));

                    if (elementHasKnownType(thisChild))
                    {
                        // we have a custom deserializer for this type, deserialize object, 
                        // add it as value, ignore further child processing
                        Object value = deserializeObject(thisChild);
                        generatedChild.setValue(value);
                        generatedChild.setType(de.tarent.gwt.ws.client.soap.Node.OBJECT_NODE);
                    }
                    else
                    {
                        // we do not know this type (yet), do simple Element archetype, then
                        // enter the subtree to look further for known type childs.                        
                        createChildElements(generatedChild, thisChild.getChildNodes());
                    }
                    root.addChild(generatedChild);
                }                
            }
        
        return root;
    }

    private boolean elementHasKnownType(Node node)
    {
        String type = getSchemaType(node);
        
        if (getDeserializerForType(type)!=null)
            return true;
        else
            return false;
    }

    private String getSchemaType(Node node)
    {
        NamedNodeMap nodeMap = node.getAttributes();
        
        for (int i=0; i<nodeMap.getLength(); i++)
        {
            Node thisAttribute = nodeMap.item(i);
            if (XSI_NAMESPACE.equals(thisAttribute.getNamespaceURI()) && "type".equals(thisAttribute.getNodeName()))
                return thisAttribute.getNodeValue();
        }

        return null;
    }

    private Element lookupElement(Element root, String namespace, String name)
    {
        NodeList childs = root.getChildNodes();
        for (int i=0; i<childs.getLength(); i++)
        {
            Node thisChild = childs.item(i);
            String thisNamespace = thisChild.getNamespaceURI();
            String thisName = SerializerUtils.mangleElementName(thisChild.getNodeName());
            
            if (thisChild.getNodeType() == Node.ELEMENT_NODE 
                    && namespace.equals(thisNamespace) 
                    && name.equals(thisName))
                return (Element)thisChild;
        }
        
        return null;
    }
    
    private Deserializer getDeserializerForType(String type)
    {
        return SoapFactory.getSerializerSettings().getDeserializer(type);
    }

    private Object deserializeObject(String type, Node node)
    {
        return getDeserializerForType(type).deserialize(node);
    }        

    private Object deserializeObject(Node node)
    {
        return deserializeObject(getSchemaType(node), node);
    }            
    
    private Serializer getSerializerForType(String type)
    {
        return SoapFactory.getSerializerSettings().getSerializer(type);
    }

    private Node serializeObject(Document ownerDocument, String classname, Object object)
    {
        return getSerializerForType(classname).serialize(ownerDocument, object);
    } 
}
