/*
 * GWT WebService,
 * GWT WebService Implementation
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'GWT WebService'
 * Signature of Elmar Geese, 25 August 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.gwt.ws.client.soap;

/**
 * Simple implementation of a SOAP fault. This currently only
 * implements the mandatory elements of a SOAP 1.2 fault.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public class Fault extends Element
{
    public static String namespace_SOAP11 = Envelope.namespace_SOAP11;
    public static String namespace_SOAP12 = Envelope.namespace_SOAP12;
 
    // TODO: this implements a rather small subset of SOAP 1.2 faults. Extend it!
    
    private String codeValue = null;
    private String reasonText = null;

    /**
     * Creates a new SOAP fault.
     * 
     * @throws NodeException
     */
    public Fault(QName codeValue, String reason) throws NodeException
    {
        super();
    }

    public String getCodeValue()
    {
        return codeValue;
    }

    public void setCodeValue(String codeValue)
    {
        this.codeValue = codeValue;
    }

    public String getReasonText()
    {
        return reasonText;
    }

    public void setReasonText(String reasonText)
    {
        this.reasonText = reasonText;
    }
    
    /**
	 * @see de.tarent.gwt.ws.client.soap.Node#addChild(int, de.tarent.gwt.ws.client.soap.Node)
	 */
	@Override
	public void addChild(int index, Node child) throws NodeException {
		
		throw new NodeException("Can't add childs to a fault element.");
	}

	/**
	 * @see de.tarent.gwt.ws.client.soap.Element#addAttribute(de.tarent.gwt.ws.client.soap.Attribute)
	 */
	@Override
	public void addAttribute(Attribute attribute) throws NodeException {
		
		throw new NodeException("Can't add attributes to a fault element.");
	}
}
