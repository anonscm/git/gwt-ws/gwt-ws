/*
 * GWT WebService,
 * GWT WebService Implementation
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'GWT WebService'
 * Signature of Elmar Geese, 25 August 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.gwt.ws.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * A simple proxy which forwards http-requests to an URL embedded in the given URL
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class ProxyServlet extends HttpServlet
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2288114978027213343L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException	{
		this.doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	

		response.setContentType("text/plain");

		try {
			
			BufferedReader in = new BufferedReader(new InputStreamReader(getRealURL(request).openStream()));
			
			while (in.ready())
				response.getWriter().println(in.readLine());
			
			response.flushBuffer();
			
		} catch(MalformedURLException excp) {
			response.sendError(404, excp.getMessage());
		}
	}

	private URL getRealURL(HttpServletRequest request) throws MalformedURLException {
		try {
			
			return new URL(request.getParameter("url"));
			
		} catch(IndexOutOfBoundsException excp) {
			throw new MalformedURLException("Given URL does not contain a real URL");
		}
	}
}
