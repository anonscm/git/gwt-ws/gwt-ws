/**
 * 
 */
package de.tarent.gwt.ws.client;

import de.tarent.gwt.ws.client.soap.SoapException;

/**
 * An enumeration of SOAP versions and their peculiarities.
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public enum SoapVersion {

	SOAP_VERSION_1_1 (1, 1, "http://schemas.xmlsoap.org/soap/envelope/", "text/xml; charset=utf-8"),
    SOAP_VERSION_1_2 (1, 2, "http://www.w3.org/2003/05/soap-envelope", "application/soap+xml; action=\"\"; charset=utf-8");
	
	private final int majorVersion;
	private final int minorVersion;
	private final String envelopeNamespace;
	private final String contentType;
	
	SoapVersion(int majorVersion, int minorVersion, String envelopeNamespace, String contentType) {
		
		this.majorVersion = majorVersion;
		this.minorVersion = minorVersion;
		this.envelopeNamespace = envelopeNamespace;
		this.contentType = contentType;
	}
	
	public int majorVersion() {
		
		return this.majorVersion;
	}
	
	public int minorVersion() {
		
		return this.minorVersion;
	}
	
	public String envelopeNamespace() {
		
		return envelopeNamespace;
	}
	
	public String headerNamespace() {
		
		return envelopeNamespace;
	}
	
	public String bodyNamespace() {
		
		return envelopeNamespace;
	}
	
	public String contentType() {
		
		return contentType;
	}
	
	static public SoapVersion getSoapVersionForNamespaceURI(String namespaceURI) throws SoapException {
		
		for(SoapVersion version : SoapVersion.values()) {
		
			if(version.envelopeNamespace.equals(namespaceURI))
				return version;
		}
		
		
        throw new SoapException("Encountered unknown envelope namespace " 
                + namespaceURI + "."); 
	}
}
