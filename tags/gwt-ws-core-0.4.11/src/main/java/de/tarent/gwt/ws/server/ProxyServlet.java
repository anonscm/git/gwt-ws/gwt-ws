/*
 * GWT WebService,
 * GWT WebService Implementation
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'GWT WebService'
 * Signature of Elmar Geese, 25 August 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.gwt.ws.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * A simple proxy which forwards http-requests to an URL embedded in the given URL
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn; Martin Pelzer (m.pelzer@tarent.de) tarentGmbH Bonn
 *
 */
public class ProxyServlet extends HttpServlet
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2288114978027213343L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException	{
		this.doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	

		response.setContentType("text/xml");
		
		BufferedReader in = null;

		try {
			// the body part of the request
			ServletInputStream requestBody = request.getInputStream();
			
			// our target url and the connection to it
			URL targetUrl = getRealURL(request);
			
			HttpURLConnection con = (HttpURLConnection) targetUrl.openConnection();
			con.setDoOutput(true);
			
			// Set HTTP-header fields
			// FIXME: should copy all header-fields from original HTTP-message except for
			// the ones which got invalid (e.g. size may have changed)
			
            con.addRequestProperty("Content-Type", request.getContentType());

			con.addRequestProperty("Accept", "application/soap+xml, application/dime, multipart/related, text/*");
	       
	        // neeeded by octopus (http://evolvis.org/projects/octopus)
	        // SOAP 1.1 style
			con.addRequestProperty("SOAPAction", "\"\"");
			
			// copy body of request to our new request
			int t;
			while ((t = requestBody.read()) != -1 ) {
				con.getOutputStream().write(t);
			}

			// get answer from new request and write in BufferedReader
			in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			
			// write buffered answer into response body
			while (in.ready())
				response.getWriter().println(in.readLine());
			
			// We're done.
			response.flushBuffer();
			
		} catch(MalformedURLException excp) {
			
			response.sendError(400, excp.getMessage());
			
		} finally {
			
			if(in != null)
				in.close();
		}
	}

	private URL getRealURL(HttpServletRequest request) throws MalformedURLException {
		try 
		{
			String url = request.getParameter("url");

			Map<String, String[]> pMap = request.getParameterMap();
			for (Map.Entry<String, String[]> entry : pMap.entrySet())
			{
				String[] value = entry.getValue();
				
				if(!entry.getKey().toLowerCase().trim().equals("url"))
					url = url + "&" + entry.getKey() + "=" + value[0];
			}
			return new URL(url);	
		} 
		catch(IndexOutOfBoundsException excp) 
		{
			throw new MalformedURLException("Given URL does not contain a real URL");
		}
	}
}
