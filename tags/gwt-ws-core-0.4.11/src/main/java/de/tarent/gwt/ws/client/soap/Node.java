/*
 * GWT WebService,
 * GWT WebService Implementation
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'GWT WebService'
 * Signature of Elmar Geese, 25 August 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.gwt.ws.client.soap;

import java.util.ArrayList;
import java.util.List;

/**
 * Top level class for all nodes.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public class Node
{
    public static final short TEXT_NODE = 0;
    public static final short ELEMENT_NODE = 1;
    public static final short ATTRIBUTE_NODE = 2;
    public static final short OBJECT_NODE = 3;
    
    private List<Node> childs = null;
    private Object value = null;
    private short type = -1;

    public List<Node> getChilds()
    {
        return childs;
    }

    public void setChilds(List<Node> childs) throws NodeException
    {
        if (childs==null)
            childs = new ArrayList<Node>();
        else
            childs.clear();

        // manually add child items using addChild() to ease subclassing
        for (int i=0; i<childs.size(); i++)
            addChild(childs.get(i));
    }

    public void addChild(Node child) throws NodeException
    {
        if (childs==null)
            childs = new ArrayList<Node>();

        addChild(getChilds().size(), child);
    }

    public void addChild(int index, Node child) throws NodeException
    {
        // if you subclass Element, just overload this method, add checks.
        // all other add methods use this method to actually add nodes.
        if (childs==null)
            childs = new ArrayList<Node>();

        getChilds().add(index, child);
    }

    public boolean removeChild(Node child)
    {
        return getChilds().remove(child);
    }

    public Object getValue()
    {
        return value;
    }

    public void setValue(Object value)
    {
        this.value = value;
    }

    public short getType()
    {
        return type;
    }

    public void setType(short type)
    {
        this.type = type;
    }
    
    public String toString()
    {
        StringBuffer buf = new StringBuffer();
        if (type==ATTRIBUTE_NODE) buf.append("[ATTRIBUTE_NODE]");
        if (type==ELEMENT_NODE) buf.append("[ELEMENT_NODE]");
        if (type==TEXT_NODE) buf.append("[TEXT_NODE]");
        if (type==OBJECT_NODE) buf.append("[OBJECT_NODE]");
        
        return buf.toString();
    }
}
