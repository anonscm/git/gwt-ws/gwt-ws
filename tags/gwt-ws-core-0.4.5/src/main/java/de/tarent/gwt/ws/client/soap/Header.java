/*
 * GWT WebService,
 * GWT WebService Implementation
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'GWT WebService'
 * Signature of Elmar Geese, 25 August 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 21.12.2007
 */

package de.tarent.gwt.ws.client.soap;

/**
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 * 
 */
public class Header extends Element
{
    public static String namespace_SOAP11 = Envelope.namespace_SOAP11;
    public static String namespace_SOAP12 = Envelope.namespace_SOAP12;
    public static String name = "Header";

    public Header() throws NodeException
    {
        super();
    }
    
    public void addChild(int index, NamedNode child) throws SoapException
    {
        if (!child.isQualified())
            throw new SoapException("Top level elements in SOAP headers must be namespace-qualified: " + child.getName());
        
        getChilds().add(child);        
    }
    
    public void addAttribute(Attribute attribute) throws SoapException
    {
        if (!attribute.isQualified())
            throw new SoapException("Top level attributes in SOAP headers must be namespace-qualified: " + attribute.getName());
        
        getChilds().add(attribute);        
    }
}
