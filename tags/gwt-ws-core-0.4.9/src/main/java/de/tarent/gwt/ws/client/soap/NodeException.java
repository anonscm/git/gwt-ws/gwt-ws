/*
 * GWT WebService,
 * GWT WebService Implementation
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'GWT WebService'
 * Signature of Elmar Geese, 25 August 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.gwt.ws.client.soap;

/**
 * Toplevel class of all tree exceptions.
 * 
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public class NodeException extends Exception
{
    /**
	 * 
	 */
	private static final long serialVersionUID = -866904134269526190L;

	public NodeException()
    {
        super();
    }
    
    public NodeException(String msg)
    {
        super(msg);
    }

    public NodeException(String msg, Throwable cause)
    {
        super(msg, cause);
    }

    public NodeException(Throwable cause)
    {
        super(cause);
    }
}
