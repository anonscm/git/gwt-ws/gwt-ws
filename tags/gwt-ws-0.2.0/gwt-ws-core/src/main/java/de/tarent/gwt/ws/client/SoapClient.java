/*
 * GWT WebService,
 * GWT WebService Implementation
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'GWT WebService'
 * Signature of Elmar Geese, 25 August 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.gwt.ws.client;

import java.util.List;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.XMLParser;

import de.tarent.gwt.ws.client.soap.Element;
import de.tarent.gwt.ws.client.soap.Node;
import de.tarent.gwt.ws.client.soap.NodeException;
import de.tarent.gwt.ws.client.soap.QName;
import de.tarent.gwt.ws.client.soap.SoapException;
import de.tarent.gwt.ws.client.soap.SoapMessage;

/**
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 * 
 */
public class SoapClient
{
    private String proxy = null;

    /**
     * Implements the wrapper callback listener for the actual async HTTP request
     * to the server. This wrapper retrieves the response xml string stream and 
     * de-serializes its contents, returning the result to the original client.
     *
     * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
     */
    private class RequestCallbackImpl implements RequestCallback
    {
        private SoapRequestCallback parentCallbackHandler = null;
        
        public RequestCallbackImpl(SoapRequestCallback callbackHandler) throws SoapException
        {
            if (callbackHandler==null)
                throw new SoapException("CallbackHandler can not be null.");
            else
            {
                this.parentCallbackHandler = callbackHandler;
            }
        }
        
        public void onError(Request request, Throwable exception)
        {
            this.parentCallbackHandler.onError(exception);
        }

        public void onResponseReceived(Request request, final Response response) 
        {
          if (200 == response.getStatusCode()) 
          {
              SoapMessage soap = null;
              Document xmlDoc = XMLParser.parse(response.getText());
              try
              {
                   soap = new SoapMessage(xmlDoc.getDocumentElement());
              }
              catch (NodeException e)
              {
                  this.parentCallbackHandler.onError(e);
                  return;
              }
              
              this.parentCallbackHandler.onResponseReceived(soap);              
          }
          else 
              this.parentCallbackHandler.onError(new SoapException("Request Error: " + response.getStatusCode() + " - " + response.getStatusText()));
        }        
    }

    public void setProxy(String proxyUrl)
    {
    	this.proxy = proxyUrl;
    }
    
    /**
     * Performs an RPC style request. Convenience method. This should only
     * be used if there is no generated code available or it is used by the
     * generated code.
     * 
     * @param serviceUrl The service url.
     * @param operation Operation name.
     * @param parameters Parameters, list of Entry objects.
     */
    public void request(SoapRequestCallback clientCallbackListener, String serviceUrl, String operation, List parameters) throws SoapException
    {
        SoapMessage soap = null;
        try
        {
            soap = new SoapMessage(SoapMessage.SOAP_VERSION_12);
        }
        catch (NodeException e)
        {
            throw new SoapException(e);
        }

        // add content to body
        // FIXME: fix namespace
        try
        {
            Element operationBody = new Element(new QName("http://namespace/", "local:" + operation));
            soap.getBody().addChild(operationBody);

            Node contentNode = new Node();
            contentNode.setType(Node.OBJECT_NODE);
            contentNode.setValue(parameters);
            operationBody.addChild(contentNode);
        }
        catch (NodeException e)
        {
            throw new SoapException(e);
        }

        sendRequest(serviceUrl + "/" + operation, soap.toString(), clientCallbackListener);
    }
    
    /**
     * Performs a simple request without any attachments. The receiving server is expected to
     * return a valid SOAP message. This operation should <b>only</b> be used for debugging purposes
     * when doing a query to a static SOAP message xml file stored on the server. <b>Do not</b> use this
     * for production!
     * 
     * @param url URL of static SOAP xml file.
     */
    public void request(SoapRequestCallback clientCallbackListener, String url) throws SoapException
    {
        sendRequest(url, null, clientCallbackListener);
    }

    /**
     * This method starts the actual request to the server. It takes an already serialized SOAP
     * document and submits it to the server for processing.
     * 
     * @param url The service URL.
     * @param attachment SOAP XML, serialized.
     * @param clientCallbackHandler The client's callback listener.
     * @throws SoapException If anything unusual goes wrong.
     */
    private void sendRequest(String url, String attachment, SoapRequestCallback clientCallbackHandler) throws SoapException
    {
    	if (proxy!=null)
    	{
    		// we're using a request proxy
    		url = this.proxy + "?url=" + url;
    	}
    	
        // FIXME: Debug
//    	Window.alert(url + " - " + attachment);        

        RequestBuilder builder = new RequestBuilder(RequestBuilder.POST, URL.encode(url));

        try 
        {
            RequestCallback callbackHandler = new RequestCallbackImpl(clientCallbackHandler);
            builder.sendRequest(attachment, callbackHandler);
        } 
        catch (RequestException e) 
        {
            clientCallbackHandler.onError(new SoapException("Cant connect: " + e.getMessage()));
        }
    }
}
