<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>org.evolvis.gwt</groupId>
	<artifactId>gwt-ws</artifactId>
	<version>0.4.3</version>
	<name>GWT WebService</name>
	<description>GWT WebService Implementation</description>
	<url>http://evolvis.org/projects/gwt-ws/</url>
	<inceptionYear>2008</inceptionYear>
	<packaging>pom</packaging>

	<modules>
		<module>gwt-ws-core</module>
		<module>gwt-ws-demo</module>
		<module>gwt-ws-octopus</module>
		<module>gwt-ws-ext</module>
	</modules>

	<licenses>
		<license>
			<name>GNU General Public License</name>
			<url>http://www.gnu.org/licenses/gpl-2.0.txt</url>
		</license>
	</licenses>
	<developers>
		<developer>
			<id>mklein</id>
			<name>Michael Kleinhenz</name>
			<email>m.kleinhenz (at) tarent.de</email>
			<organization>tarent GmbH</organization>
			<organizationUrl>http://www.tarent.de/</organizationUrl>
			<roles>
				<role>Developer</role>
			</roles>
			<timezone>+1</timezone>
		</developer>
		<developer>
			<id>mpelze</id>
			<name>Martin Pelzer</name>
			<email>m.pelzer (at) tarent.de</email>
			<organization>tarent GmbH</organization>
			<organizationUrl>http://www.tarent.de/</organizationUrl>
			<roles>
				<role>Developer</role>
			</roles>
			<timezone>+1</timezone>
		</developer>
		<developer>
			<id>fkoest</id>
			<name>Fabian Köster</name>
			<email>f.koester (at) tarent.de</email>
			<organization>tarent GmbH</organization>
			<organizationUrl>http://www.tarent.de/</organizationUrl>
			<roles>
				<role>Developer</role>
			</roles>
			<timezone>+1</timezone>
		</developer>
	</developers>
	<organization>
		<name>tarent GmbH, Bonn, Germany</name>
		<url>http://www.tarent.de/</url>
	</organization>

	<reporting>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>
					maven-project-info-reports-plugin
				</artifactId>
				<reportSets>
					<reportSet>
						<reports>
							<report>dependencies</report>
							<report>project-team</report>
							<!--<report>mailing-list</report>-->
							<!--<report>cim</report>-->
							<!--<report>issue-tracking</report>-->
							<!--<report>license</report>-->
							<!--<report>scm</report>-->
						</reports>
					</reportSet>
				</reportSets>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<configuration>
					<aggregate>true</aggregate>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-changelog-plugin</artifactId>
			</plugin>
		</plugins>
	</reporting>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-site-plugin</artifactId>
				<configuration>
					<inputEncoding>UTF-8</inputEncoding>
					<outputEncoding>UTF-8</outputEncoding>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-release-plugin</artifactId>
				<configuration>
					<preparationGoals>install</preparationGoals>
				</configuration>
			</plugin>
			<plugin>
				<groupId>de.tarent.maven.plugins</groupId>
				<artifactId>mvn-gforge-plugin</artifactId>
				<configuration>
					<gForgeProject>gwt-ws</gForgeProject>
					<gForgeAPIPort_address>http://evolvis.org/soap/index.php</gForgeAPIPort_address>
					<fileTypes>
						<type>javadoc</type>
						<type>sources</type>
					</fileTypes>
				</configuration>
			</plugin>
		</plugins>
	</build>
	
	<repositories>
		<repository>
			<id>evolvis-release-repository</id>
			<name>evolvis.org release repository</name>
			<url>http://maven-repo.evolvis.org/releases</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>evolvis-snapshot-repository</id>
			<name>evolvis.org snapshot repository</name>
			<url>http://maven-repo.evolvis.org/snapshots</url>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
			<releases>
				<enabled>false</enabled>
			</releases>
		</repository>
		<repository>
			<id>gwt-maven</id>
			<name>gwt-maven repository</name>
			<url>http://gwt-maven.googlecode.com/svn/trunk/mavenrepo/</url>
		</repository> 
	</repositories>
	
	<pluginRepositories>
		<pluginRepository>
			<id>evolvis-release-repository</id>
			<name>evolvis.org release repository</name>
			<url>http://maven-repo.evolvis.org/releases</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</pluginRepository>
		<pluginRepository>
			<id>evolvis-snapshot-repository</id>
			<name>evolvis.org snapshot repository</name>
			<url>http://maven-repo.evolvis.org/snapshots</url>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
			<releases>
				<enabled>false</enabled>
			</releases>
		</pluginRepository>
		<pluginRepository>
			<id>gwt-maven</id>
			<name>gwt-maven repository</name>
			<url>http://gwt-maven.googlecode.com/svn/trunk/mavenrepo/</url>
		</pluginRepository> 
	</pluginRepositories>
	
	
	<!-- this section is required for deployment -->
	<distributionManagement>
		<repository>
			<id>evolvis-release-repository</id>
			<name>evolvis.org release repository</name>
			<url>
				scpexe://maven-repo.evolvis.org:/var/www/maven_repo/releases</url>
		</repository>
		<snapshotRepository>
			<id>evolvis-snapshot-repository</id>
			<name>evolvis.org snapshot repository</name>
			<url>
				scpexe://maven-repo.evolvis.org:/var/www/maven_repo/snapshots</url>
		</snapshotRepository>
		<site>
			<id>evolvis-site-repository</id>
			<name>evolvis.org site repository</name>
			<url>
				scpexe://maven@svn.evolvis.org:/site/gwt-ws/htdocs/site/gwt-ws</url>
		</site>
	</distributionManagement>
	
	<issueManagement>
		<system>Evolvis</system>
		<url>http://evolvis.org/tracker/?group_id=48</url>
	</issueManagement>
	
	<mailingLists>
		<mailingList>
			<name>gwt-ws-commits</name>
			<subscribe>
				http://lists.evolvis.org/cgi-bin/mailman/listinfo/gwt-ws-commits</subscribe>
			<unsubscribe>
				http://lists.evolvis.org/cgi-bin/mailman/listinfo/gwt-ws-commits</unsubscribe>
			<archive>
				http://lists.evolvis.org/pipermail/gwt-ws-commits/</archive>
		</mailingList>
	</mailingLists>
	
	<scm>
		<connection>scm:svn:svn://svn.evolvis.org/svnroot/gwt-ws/tags/gwt-ws-0.4.3</connection>
		<developerConnection>scm:svn:svn+ssh://maven@svn.evolvis.org/svnroot/gwt-ws/tags/gwt-ws-0.4.3</developerConnection>
		<url>http://evolvis.org/plugins/scmsvn/viewcvs.php/trunk?root=gwt-ws/tags/gwt-ws-0.4.3?root=gwt-ws</url>
	</scm>
	
	<ciManagement>
		<system>continuum</system>
		<url>http://cis.evolvis.org:8080/continuum/</url>
	</ciManagement>
</project>