/*
 * GWT WebService,
 * GWT WebService Implementation
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'GWT WebService'
 * Signature of Elmar Geese, 25 August 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.gwt.ws.client.soap;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Implements a SOAP document node.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public class Element extends NamedNode
{
    private List attributes = null;
    
    /**
     * Constructs a new node using the supplied QName and childs.
     * 
     * @param qName
     * @param value
     */
    public Element(QName qName, List attributes, List childs) throws NodeException
    {
        super(qName);
        
        setType(Node.ELEMENT_NODE);
        setValue(null);

        if (attributes!=null)
            setAttributes(attributes);
        
        if (childs!=null)
            setChilds(childs);
    }

    /**
     * Constructs a new node with the given QName and an empty value.
     * 
     * @param qName
     */
    public Element(QName qName) throws NodeException
    {
        this(qName, null, null);
    }
    
    /**
     * Constructs an empty element.
     */
    public Element() throws NodeException
    {
        this(null);
    }
   
    public List getAttributes()
    {
        return attributes;
    }

    public void setAttributes(List attributes) throws NodeException
    {
        if (attributes==null)
            attributes = new ArrayList();
        else
            attributes.clear();
        
        // manually add attribute items using addAttribute() to ease subclassing
        for (int i=0; i<attributes.size(); i++)
            addAttribute((Attribute)attributes.get(i));
    }

    public void addAttribute(Attribute attribute) throws NodeException
    {
        // if you subclass Element, just overload this method, add checks.
        // all other add methods use this method to actually add attributes.
        getAttributes().add(attribute);
    }
    
    public boolean removeAttribute(Attribute attribute) 
    {
        return getAttributes().remove(attribute);
    }

    public boolean removeAttribute(QName attributeQName)
    {
        Attribute selectedAttribute = lookupAttribute(attributeQName);
        return getAttributes().remove(selectedAttribute);
    }

    public boolean removeAttribute(String attributeNamespace, String attributeName)
    {
        return removeAttribute(new QName(attributeNamespace, attributeName));
    }

    public boolean removeAttribute(String attributeName)
    {
        return removeAttribute(null, attributeName);
    }

    /**
     * Finds the attribute with the supplied name among the attributes
     * of this Element.
     * 
     * @param qName
     * @return Attribute matching the given QName.
     */
    private Attribute lookupAttribute(QName qName)
    {
        if (getAttributes()==null)
            return null;
        
        Iterator iter = getAttributes().iterator();
        while (iter.hasNext())
        {
            Attribute thisAttribute = (Attribute)iter.next();
            if (thisAttribute.getQName().equals(qName)) 
                return thisAttribute;
        }
        
        return null;
    }
}
