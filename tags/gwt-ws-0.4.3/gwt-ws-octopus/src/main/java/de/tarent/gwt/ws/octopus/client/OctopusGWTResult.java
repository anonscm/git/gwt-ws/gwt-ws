/**
 * 
 */
package de.tarent.gwt.ws.octopus.client;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class OctopusGWTResult {
	
	Map<String, Object> resultData;
	
	
	public OctopusGWTResult() {
		resultData = new HashMap<String, Object>();
	}

	/**
	 * @see de.tarent.octopus.client.OctopusResult#getContentType()
	 */
	public String getContentType() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @see de.tarent.octopus.client.OctopusResult#getData(java.lang.String)
	 */
	public Object getData(String key) {
		return resultData.get(key);
	}
	
	public void setData(String key, Object data) {
		resultData.put(key, data);
	}

	/**
	 * @see de.tarent.octopus.client.OctopusResult#getDataKeys()
	 */
	public Iterator getDataKeys() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @see de.tarent.octopus.client.OctopusResult#hasMoreData()
	 */
	public boolean hasMoreData() {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * @see de.tarent.octopus.client.OctopusResult#hasStreamContent()
	 */
	public boolean hasStreamContent() {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * @see de.tarent.octopus.client.OctopusResult#nextData()
	 */
	public Object nextData() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @see de.tarent.octopus.client.OctopusResult#nextDataAsString()
	 */
	public String nextDataAsString() {
		// TODO Auto-generated method stub
		return null;
	}
}
