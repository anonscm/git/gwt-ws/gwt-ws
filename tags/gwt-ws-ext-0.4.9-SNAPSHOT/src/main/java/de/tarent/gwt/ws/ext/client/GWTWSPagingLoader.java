/**
 * 
 */
package de.tarent.gwt.ws.ext.client;

import com.extjs.gxt.ui.client.data.BasePagingLoader;
import com.extjs.gxt.ui.client.data.DataProxy;
import com.extjs.gxt.ui.client.data.DataReader;
import com.extjs.gxt.ui.client.data.PagingLoadResult;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class GWTWSPagingLoader<T> extends BasePagingLoader< PagingLoadResult<T>> {

	public GWTWSPagingLoader(DataProxy< PagingLoadResult<T>> proxy, DataReader< PagingLoadResult<T>> reader) {
		super(proxy, reader);
	}

	@Override
	protected GWTWSPagingLoadConfig newLoadConfig() {
		return new GWTWSPagingLoadConfig();
	}
}
