/*
 * GWT WebService,
 * GWT WebService Implementation
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'GWT WebService'
 * Signature of Elmar Geese, 25 August 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 21.12.2007
 */

package de.tarent.gwt.ws.client.soap;

import com.google.gwt.xml.client.Document;

import de.tarent.gwt.ws.client.types.MapSerializerSettings;
import de.tarent.gwt.ws.client.types.SerializerSettings;

/**
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 * 
 */
public class SoapFactory
{
    private static SerializerSettings serializerSettings = new MapSerializerSettings();
    
    public static SoapMessage parseMessage(Document soap) throws NodeException
    {
        return new SoapMessage(soap.getDocumentElement());
    }

    public static void setSerializerSettings(SerializerSettings serializerSettings)
    {
        SoapFactory.serializerSettings = serializerSettings;
    }
    
    public static SerializerSettings getSerializerSettings()
    {
        return SoapFactory.serializerSettings;
    }
}
