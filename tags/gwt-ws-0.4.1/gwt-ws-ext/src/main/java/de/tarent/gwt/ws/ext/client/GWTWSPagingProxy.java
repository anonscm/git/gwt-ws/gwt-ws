package de.tarent.gwt.ws.ext.client;
import java.util.ArrayList;

import com.extjs.gxt.ui.client.data.BasePagingLoadResult;
import com.extjs.gxt.ui.client.data.DataProxy;
import com.extjs.gxt.ui.client.data.DataReader;
import com.extjs.gxt.ui.client.data.PagingLoadResult;
import com.google.gwt.user.client.rpc.AsyncCallback;

import de.tarent.gwt.ws.client.SoapClient;
import de.tarent.gwt.ws.client.SoapRequestCallback;
import de.tarent.gwt.ws.client.soap.SoapException;
import de.tarent.gwt.ws.client.soap.SoapMessage;

/**
 * 
 */

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class GWTWSPagingProxy<E> implements DataProxy<GWTWSPagingLoadConfig, PagingLoadResult> {
	
	private SoapClient soapClient;
	private String url;
	private String operation;
//	private List<Entry> parameters;
	
	public GWTWSPagingProxy(SoapClient soapClient) {
		this(soapClient, null, null);
	}

	public GWTWSPagingProxy(SoapClient soapClient, String url, String operation) {
		this.soapClient = soapClient;
		this.url = url;
		this.operation = operation;
	}

	public void load(final DataReader<GWTWSPagingLoadConfig, PagingLoadResult> reader, final GWTWSPagingLoadConfig loadConfig, final AsyncCallback<PagingLoadResult> callback) {
		
		System.out.println(loadConfig);
		System.out.println(loadConfig.getParameters());
		
		try {
			soapClient.request(new SoapRequestCallback() {

				public void onError(Throwable exception) {
					callback.onFailure(exception);
				}

				public void onResponseReceived(SoapMessage response) {
					
					if(response != null)
						callback.onSuccess(reader.read(loadConfig, response));
					else
						callback.onSuccess(new BasePagingLoadResult<E>(new ArrayList<E>()));
					
				}
			}, url, operation, loadConfig.getParameters());
			
		} catch (SoapException e) {
			callback.onFailure(e);
		}
	}
	
	public void setURL(String url) {
		this.url = url;
	}
	
	public void setOperation(String operation) {
		this.operation = operation;
	}
	
//	public void setParameters(List<Entry> parameters) {
//		this.parameters = parameters;
//	}
	
	public void setSoapClient(SoapClient soapClient) {
		this.soapClient = soapClient;
	}
}
