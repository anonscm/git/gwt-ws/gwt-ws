/**
 * 
 */
package de.tarent.gwt.ws.ext.client;

import java.util.List;

import com.extjs.gxt.ui.client.data.BasePagingLoadConfig;

import de.tarent.gwt.ws.client.types.Entry;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class GWTWSPagingLoadConfig extends BasePagingLoadConfig {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7272019413071200630L;
	
	List<Entry> parameters;

	public List<Entry> getParameters() {
		return parameters;
	}
	
	public void setParameters(List<Entry> parameters) {
		this.parameters = parameters;
	}
}
