/*
 * GWT WebService,
 * GWT WebService Implementation
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'GWT WebService'
 * Signature of Elmar Geese, 25 August 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.gwt.ws.client.types;

import java.util.Date;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.xml.client.Element;
import com.google.gwt.xml.client.NamedNodeMap;
import com.google.gwt.xml.client.Node;

/**
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public class SerializerUtils
{
    /**
     * Does a baselevel deserializing of a value node (an element node containing only
     * a single text node child).
     * 
     * @param node
     * @return
     */
    public static Object baselevelDeserialize(Node node)
    {
        String value = node.getFirstChild().getNodeValue();
        NamedNodeMap attributes = node.getAttributes();
        String xsiType = null;
        int i = 0;
        
        // TODO Here the xsi type is fetched from the incoming XML fragment.
        // That does not work for document style!
        
        while (attributes.getLength()!=0 && xsiType==null)
        {
            Node thisAttribute = attributes.item(i);
            if (thisAttribute.getNodeName()!=null && thisAttribute.getNodeName().endsWith(":type"))
                xsiType = mangleElementName(thisAttribute.getNodeValue());
        }

        // we have now retrieved the type, lets try deserializing it
        // fallback to string if nothing works

        Object objectValue = value;
        try
        {
            // TODO: add more deserializers for base types
            if (xsiType.equals("boolean"))
                objectValue = new Boolean(value);
            else if (xsiType.equals("int"))
                objectValue = new Integer(value);
            else if (xsiType.equals("string"))
                objectValue = new String(value);
            else if (xsiType.equals("float"))
                objectValue = new Float(value);
            else if (xsiType.equals("double"))
                objectValue = new Double(value);
            else if (xsiType.equals("long"))
                objectValue = new Long(value);
            else if (xsiType.equals("dateTime"))
                objectValue = DateTimeFormat.getFormat("yyyy-MM-dd'T'HH:mm:ss'.'SSSz").parse(value);
        }
        catch (Exception e)
        {
            // didn't work, automatic fallback to string
        }
        
        return objectValue;
    }

    /**
     * Does a baselevel serializing of a value.
     * 
     * @param object
     * @return node
     */
    public static Node baselevelSerialize(Node parent, Object object)
    {
        // FIXME: get xsi/xsd namespace prefix
        String xsiAttributeName = "xsi:type";
        String xsdPrefix = "xsd";
        
        // TODO: add more serializers for base types
        if (object instanceof Boolean)
        {
            ((Element)parent).setAttribute(xsiAttributeName, xsdPrefix + ":boolean");
            
            Node thisNode = null;
            if (((Boolean)object).booleanValue())
                 thisNode = parent.getOwnerDocument().createTextNode("true");
            else
                 thisNode = parent.getOwnerDocument().createTextNode("false");

            return thisNode;
        }
        else if (object instanceof Date)
        {
            ((Element)parent).setAttribute(xsiAttributeName, xsdPrefix + ":dateTime");
            String dateString = DateTimeFormat.getFormat("yyyy-MM-dd'T'kk':'mm':'ss'.'SSS'Z'").format((Date)object);
            Node thisNode = parent.getOwnerDocument().createTextNode(dateString);
            return thisNode;
        }
        else if(object instanceof String) {
        	 ((Element)parent).setAttribute(xsiAttributeName, xsdPrefix + ":string");
             
             Node thisNode = null;
             thisNode = parent.getOwnerDocument().createTextNode(object.toString());

             return thisNode;
        }
        else if(object instanceof Integer) {
        	((Element)parent).setAttribute(xsiAttributeName, xsdPrefix + ":int");
            
            Node thisNode = null;
            thisNode = parent.getOwnerDocument().createTextNode(object.toString());

            return thisNode;
        }
        else
        {
            // Fallback to .toString without type
            Node thisNode = parent.getOwnerDocument().createTextNode(object.toString());
            return thisNode;
        }
    }

    public static boolean isValueNode(Node node)
    {
        return node.getChildNodes().getLength()==1 
                && node.getFirstChild().getNodeType() == Node.TEXT_NODE;
    }

    public static boolean isElementNode(Node node)
    {
        for (int i=0; i<node.getChildNodes().getLength(); i++)
            if (node.getChildNodes().item(i).getNodeType() == Node.ELEMENT_NODE)
                return true;

        return false;
    }
    
    public static String mangleElementName(String name)
    {
        return name.substring(name.indexOf(':')+1);
    }
}
