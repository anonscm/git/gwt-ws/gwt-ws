/**
 * 
 */
package de.tarent.gwt.ws.ext.client;

import com.extjs.gxt.ui.client.data.BasePagingLoader;
import com.extjs.gxt.ui.client.data.DataProxy;
import com.extjs.gxt.ui.client.data.DataReader;
import com.extjs.gxt.ui.client.data.PagingLoadResult;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class GWTWSPagingLoader<D extends PagingLoadResult> extends BasePagingLoader<GWTWSPagingLoadConfig, D> {

	public GWTWSPagingLoader(DataProxy<GWTWSPagingLoadConfig, D> proxy, DataReader reader) {
		super(proxy, reader);
	}

	@Override
	protected GWTWSPagingLoadConfig newLoadConfig() {
		return new GWTWSPagingLoadConfig();
	}
}
