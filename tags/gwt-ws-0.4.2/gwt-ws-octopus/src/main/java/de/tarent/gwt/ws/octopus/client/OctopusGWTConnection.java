/**
 * 
 */
package de.tarent.gwt.ws.octopus.client;

import java.util.List;

import de.tarent.gwt.ws.client.SoapClient;
import de.tarent.gwt.ws.client.SoapRequestCallback;
import de.tarent.gwt.ws.client.soap.SoapException;
import de.tarent.gwt.ws.client.soap.SoapMessage;
import de.tarent.gwt.ws.client.types.Entry;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class OctopusGWTConnection {
	private SoapClient soapClient;
	private String url;

	public OctopusGWTConnection(String url) {
		this(url, null);
	}
	
	public OctopusGWTConnection(String url, String namespace) {
		soapClient = new SoapClient();
		this.url = url;
		
		if(namespace != null)
			soapClient.setNamespace(namespace);
	}
	
	public void setProxy(String proxy) {
		soapClient.setProxy(proxy);
	}

	public void callTask(final String taskName, List<Entry> params, final OctopusResponseListener listener) {

		
		try {
			soapClient.request(new SoapRequestCallback() {

				public void onError(Throwable exception) {
					listener.errorReceived(exception);
				}

				public void onResponseReceived(SoapMessage response) {				
					listener.responseReceived(OctopusResultParser.getResultFromSoapMessage(response, taskName));
				}

			}, url, taskName, params);
			
		} catch(SoapException excp) {
			listener.errorReceived(excp);
		}
	}
	
	public SoapClient getSoapClient() {
		return soapClient;
	}
	
	public String getURL() {
		return url;
	}
}
