/*
 * GWT WebService,
 * GWT WebService Implementation
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'GWT WebService'
 * Signature of Elmar Geese, 25 August 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.gwt.ws.client.soap;

/**
 * Implements the SOAP envelope according to SOAP 1.2 specification.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public class Envelope
{
    public static final String name = "Envelope";
    
    private Header header = null;
    private Body body = null;
    
    /**
     * Creates a SOAP envelope using the supplied header and body.
     * 
     * @param header
     * @param body
     */
    public Envelope(Header header, Body body)
    {
        super();
        this.header = header;
        this.body = body;
    }

    /**
     * Creates an empty SOAP envelope.
     */
    public Envelope()
    {
        super();
    }
    
    public Header getHeader()
    {
        return header;
    }
    
    public void setHeader(Header header)
    {
        this.header = header;
    }
    
    public Body getBody()
    {
        return body;
    }
    
    public void setBody(Body body)
    {
        this.body = body;
    }
    
    /**
     * Creates and associates a new empty SOAP header with this
     * envelope.
     * 
     * @return the newly created header.
     */
    public Header createHeader() throws SoapException
    {
        try
        {
            this.setHeader(new Header());
        }
        catch (NodeException e)
        {
            throw new SoapException(e);
        }
        
        return this.getHeader();
    }
    
    /**
     * Creates and associates a new empty SOAP body with this
     * envelope.
     * 
     * @return the newly created header.
     */
    public Body createBody() throws SoapException
    {
        try
        {
            this.setBody(new Body());
        }
        catch (NodeException e)
        {
            throw new SoapException(e);
        }
        
        return this.getBody();
    }
}
