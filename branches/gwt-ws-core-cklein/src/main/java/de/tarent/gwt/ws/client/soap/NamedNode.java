/*
 * GWT WebService,
 * GWT WebService Implementation
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'GWT WebService'
 * Signature of Elmar Geese, 25 August 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.gwt.ws.client.soap;

/**
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public abstract class NamedNode extends Node
{
    private QName qName = null;

    public NamedNode(QName qName)
    {
        super();
        setQName(qName);
    }
    
    public NamedNode()
    {
        this(null);
    }
    
    public QName getQName()
    {
        return qName;
    }

    public void setQName(QName name)
    {
        qName = name;
    }
    
    public String getName()
    {
        if (getQName()!=null)
            return getQName().getName();
        
        return null;
    }

    public String getNamespace()
    {
        if (getQName()!=null)
            return getQName().getNamespace();
        
        return null;
    }
    
    /**
     * Checks whether this Node has a qualified name (the QName of
     * the Node has a namespace).
     * 
     * @return true if the name of this Node is qualified, false otherwise.
     */
    public boolean isQualified()
    {
        return getQName().isQualified();
    }
    
    public String toString()
    {
        StringBuffer buf = new StringBuffer();
        if (getType()==ATTRIBUTE_NODE) buf.append("[ATTRIBUTE_NODE '" + getQName() + "']");
        if (getType()==ELEMENT_NODE) buf.append("[ELEMENT_NODE '" + getQName() + "']");
        if (getType()==OBJECT_NODE) buf.append("[OBJECT_NODE '" + getQName() + "']");
        
        return buf.toString();
    }
}
