/*
 * gepris,
 * GEPRIS allows to query the projects as well as the
	assigned persons and institutions which are funded by the DFG (Deutsche Forschungsgemeinschaft)
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gepris'
 * Signature of Elmar Geese, 9 December 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.gwt.ws.ext.client;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.BasePagingLoadResult;
import com.extjs.gxt.ui.client.data.DataReader;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.data.PagingLoadResult;

import de.tarent.gwt.ws.client.soap.Node;
import de.tarent.gwt.ws.client.soap.SoapMessage;
import de.tarent.gwt.ws.client.types.Entry;

/**
 * This class implements a DataReader (basically a deserializer) for Paging-Tables using gwt-ws for data-retrieval.
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public abstract class AbstractDataModelReader<T extends ModelData> implements DataReader<PagingLoadResult<T>> {

	/**
	 * the expected id of the SOAP-element containing the result-list
	 */
	private String entityListName;
	
	/**
	 * the expected id of the SOAP-element containing the size of the result-list
	 */
	private String sizeName;
	
	
	/**
	 * Constructs an instance of AbstactDataModelReader
	 * 
	 * @param entityListName - the expected id of the SOAP-element containing the result-list
	 * @param sizeName - the expected id of the SOAP-element containing the size of the result-list
	 */
	public AbstractDataModelReader(String entityListName, String sizeName) {
		
		this.entityListName = entityListName;
		this.sizeName = sizeName;
	}
	
	/**
	 * 
	 * @see com.extjs.gxt.ui.client.data.DataReader#read(java.lang.Object, java.lang.Object)
	 */
	private PagingLoadResult<T> read_(GWTWSPagingLoadConfig loadConfig, Object data) {	
		List<T> entityList = new ArrayList<T>();
		
		int size = getEntitiesFromNode(((SoapMessage)data).getBody(), entityList);

		BasePagingLoadResult<T> result = new BasePagingLoadResult<T>(entityList, loadConfig.getOffset(), size);

		return result;
	}
	public PagingLoadResult<T> read(Object loadConfig, Object data) {
		
		return read_((GWTWSPagingLoadConfig) loadConfig, data);
	}
	
	/**
	 * This method parses the XML-tree of a SOAP-response and creates a list of the desired target-beans.
	 * 
	 * @param node - the node containing the result-list
	 * @param listToAddTo - the list to which the beans should be added
	 * @return the size of the result-list (should be same as listToAddTo.size() after this method has been called)
	 */
	private int getEntitiesFromNode(Node node, List<T> listToAddTo) {
		
		return getEntitiesFromNode(node, listToAddTo, -1);
	}

	/**
	 * This method parses the XML-tree of a SOAP-response and creates a list of the desired target-beans.
	 * 
	 * This method should not be invoked directly, use getEntitiesFromNode(Node node, List<T> listToAddTo).
	 * 
	 * @param node - the node containing the result-list
	 * @param listToAddTo - the list to which the beans should be added
	 * @param size - the size of the result-list.
	 * @return
	 */
	private int getEntitiesFromNode(Node node, List<T> listToAddTo, int size) {

		if(node.getType() == Node.OBJECT_NODE) {

			if(node.getValue() instanceof List) {

				List<?> responseList = (List<?>)node.getValue();

				for(Object response : responseList) {

					if(response instanceof Entry) {
						Entry entry = (Entry)response;

						if(entry.getKey().equals(sizeName))
							size = (Integer)entry.getValue();
						
						else if(entry.getKey().equals(entityListName)) {

							if(entry.getValue() instanceof List) {
								List<?> entityList = (List<?>)entry.getValue();

								for(Object entityEntry : entityList) {

									if(entityEntry instanceof Entry) {

										if(((Entry)entityEntry).getValue() instanceof List) {
											List<?> entityAttributeList = (List<?>)((Entry)entityEntry).getValue();

											T entity = createNewEntityInstace();

											// Iterate through the attributes of this bean 
											for(Object attribute : entityAttributeList) {

												if(attribute instanceof Entry)
													entity.set(((Entry)attribute).getKey(), ((Entry)attribute).getValue());
												
											}
											
											// Add the created bean to the result-list
											listToAddTo.add(entity);
										}

									}
								}
							}
						}
					}
				}
			}
		}

		if (node.getChilds() != null) {

			for(Object child : node.getChilds())
				size = getEntitiesFromNode((Node)child, listToAddTo, size);

		} else if(node.getValue() instanceof List) {

			for(Object subNode : (List<?>)node.getValue())
				if(subNode instanceof Node)
					size = getEntitiesFromNode((Node)subNode, listToAddTo, size);
		}

		if(size < listToAddTo.size())
			size = listToAddTo.size();

		return size;
	}
	
	protected abstract T createNewEntityInstace();
}
