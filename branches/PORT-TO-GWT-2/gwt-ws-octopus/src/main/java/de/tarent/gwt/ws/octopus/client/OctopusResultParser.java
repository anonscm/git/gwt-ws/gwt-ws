/**
 * 
 */
package de.tarent.gwt.ws.octopus.client;

import java.util.List;

import de.tarent.gwt.ws.client.soap.Element;
import de.tarent.gwt.ws.client.soap.Node;
import de.tarent.gwt.ws.client.soap.SoapMessage;
import de.tarent.gwt.ws.client.types.Entry;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class OctopusResultParser {

	public static OctopusGWTResult getResultFromSoapMessage(SoapMessage message, String taskName) {
		OctopusGWTResult result = new OctopusGWTResult();
		List<Node> childs = message.getBody().getChilds();
		
		for(Node node : childs) {
			
			if(node instanceof Element) {
				Element element = (Element)node;
				
				if(element.getName().equals(taskName+"Response")) {
							
					if(element.getValue() instanceof List) {
						
						List elementList = (List)element.getValue();
						
						for(Object listObject : elementList) {
							if(listObject instanceof Entry) {
								Entry entry = (Entry)listObject;
								result.setData(entry.getKey(), entry.getValue());
							}
						}
					}
				}
			}
		}
		
		return result;
	}
}
