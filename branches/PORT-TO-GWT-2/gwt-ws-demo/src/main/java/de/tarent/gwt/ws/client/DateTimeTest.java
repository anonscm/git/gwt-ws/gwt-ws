/**
 * 
 */
package de.tarent.gwt.ws.client;

import java.util.Date;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import de.tarent.gwt.ws.client.types.XSIDateTimeDeserializer;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class DateTimeTest implements EntryPoint 
	{
		public void onModuleLoad() 
		{
			final TextBox text = new TextBox();
			
			Button button = new Button("Parse");

			button.addClickListener(new ClickListener() 
			{
				public void onClick(Widget sender) 
				{
					try {
						
						Date date = XSIDateTimeDeserializer.parseDateTime(text.getText());
						Window.alert("Date and Time relative to UTC:\n"+date.toGMTString());
						
					} catch(IllegalArgumentException excp) {
						
						Window.alert(excp.getMessage());
					}
					
					
				}
			});

			RootPanel.get("targets").add(text);
			RootPanel.get("button").add(button);
		}
}
