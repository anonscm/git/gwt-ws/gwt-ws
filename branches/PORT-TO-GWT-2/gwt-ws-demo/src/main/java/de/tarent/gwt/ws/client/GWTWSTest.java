/*
 * GWT WebService,
 * GWT WebService Implementation
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'GWT WebService'
 * Signature of Elmar Geese, 25 August 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.gwt.ws.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

import de.tarent.gwt.ws.client.soap.SoapException;
import de.tarent.gwt.ws.client.soap.SoapMessage;
import de.tarent.gwt.ws.client.types.Entry;

/**
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 * 
 */
public class GWTWSTest implements EntryPoint 
{
	public void onModuleLoad() 
	{
		final Button button = new Button("Send Query");
		final ListBox targets = new ListBox();

		targets.addItem("Generate SOAP Query", "");
		targets.addItem("Parse Simple SOAP 1.1 Response", "soap_11.xml");
		targets.addItem("Parse Simple SOAP 1.2 Response", "soap_12.xml");
		targets.setWidth("500px");

		button.addClickListener(new ClickListener() 
		{
			public void onClick(Widget sender) 
			{
				if (targets.getSelectedIndex()==0)
				{
					// SOAP Query
					SoapClient client = new SoapClient();
					client.setProxy("http://localhost:8888/de.tarent.gwt.ws.GWTWSTest/proxy/");

					List demoList = new ArrayList();
					demoList.add(new Entry("demodate", new Date()));
					try
					{
						client.request(new SoapRequestCallback() {

							public void onError(Throwable exception)
							{
								Window.alert("Error: " + exception.getMessage());            
							}

							public void onResponseReceived(SoapMessage response)
							{
								Window.alert("Response: " + response.toString());            
							}                  
						}, "http://some/server/", "operation1", demoList);
					}
					catch (SoapException exception)
					{
						Window.alert("Error: " + exception.getMessage());            
					}
				}
				else
				{          
					// Read SOAP Response

					SoapClient client = new SoapClient();
					String url = targets.getValue(targets.getSelectedIndex());
					try
					{
						client.request(new SoapRequestCallback() {

							public void onError(Throwable exception)
							{
								Window.alert("Error: " + exception.getMessage());
							}

							public void onResponseReceived(SoapMessage response)
							{
								String dataString = response.getBody().getDeserializedData().toString();
								if (dataString.length()<500)
									Window.alert("Done getting and deserializing SOAP Message, " + dataString.length()+ " Characters in result.\n\n" + "Sample Data:\n" + dataString);                     
								else
									Window.alert("Done getting and deserializing SOAP Message, " + dataString.length()+ " Characters in result.\n\n" + "Sample Data:\n" + dataString.substring(0, 500) + " [continued..] ");                     
							}

						}, url);
					}
					catch (SoapException exception)
					{
						Window.alert("Error: " + exception.getMessage());            
					}
				}
			}
		});

		RootPanel.get("button").add(button);
		RootPanel.get("targets").add(targets);
	}
}
