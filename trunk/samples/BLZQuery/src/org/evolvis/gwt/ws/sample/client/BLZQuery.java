package org.evolvis.gwt.ws.sample.client;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.Events;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.data.BaseModel;
import com.extjs.gxt.ui.client.data.BasePagingLoadResult;
import com.extjs.gxt.ui.client.data.DataReader;
import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.data.Loader;
import com.extjs.gxt.ui.client.data.PagingLoadResult;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.PagingToolBar;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;

import de.tarent.gwt.ws.client.SoapClient;
import de.tarent.gwt.ws.client.soap.Node;
import de.tarent.gwt.ws.client.soap.SoapMessage;
import de.tarent.gwt.ws.client.types.Entry;
import de.tarent.gwt.ws.ext.client.GWTWSPagingLoadConfig;
import de.tarent.gwt.ws.ext.client.GWTWSPagingLoader;
import de.tarent.gwt.ws.ext.client.GWTWSPagingProxy;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class BLZQuery implements EntryPoint {

	Grid<Bank> bankGrid;
	GWTWSPagingLoader<Bank> loader;
	GWTWSPagingProxy<Bank> proxy;
	PagingToolBar toolBar;
	SoapClient soapClient;
	TextField<String> blzInput;

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {

		soapClient = new SoapClient();

		soapClient.setProxy(GWT.getHostPageBaseURL() + "proxy");
		soapClient.setNamespace("http://thomas-bayer.com/blz/");

		proxy = new GWTWSPagingProxy<Bank>(soapClient,
				"http://www.thomas-bayer.com/axis2/services/BLZService/",
		"getBank");

		BankListDataReader dataReader = new BankListDataReader();

		loader = new GWTWSPagingLoader<Bank>(proxy, dataReader);
		loader.setRemoteSort(true);
		loader.addListener(Loader.BeforeLoad, new Listener<LoadEvent>() {

			public void handleEvent(LoadEvent le) {
				GWTWSPagingLoadConfig config = (GWTWSPagingLoadConfig)le.config;

				List<Entry> searchParams = new ArrayList<Entry>(1);

				searchParams.add(new Entry("blz", blzInput.getValue()));

				config.setParameters(searchParams);
			}

		});

		toolBar = new PagingToolBar(20);
		toolBar.bind(loader);

		List<ColumnConfig> columns = new ArrayList<ColumnConfig>();

		columns.add(new ColumnConfig("bezeichnung", "Bezeichnung", 200));
		columns.add(new ColumnConfig("bic", "BIC", 200));
		columns.add(new ColumnConfig("plz", "PLZ", 200));
		columns.add(new ColumnConfig("ort", "Ort", 200));

		ColumnModel cm = new ColumnModel(columns);

		// store  
		ListStore<Bank> store = new ListStore<Bank>(loader);

		bankGrid = new Grid<Bank>(store, cm);
		bankGrid.setHeight(100);
		
		ContentPanel panel = new ContentPanel();
		panel.setHeading("Results");
		panel.setFrame(true);  
		panel.setHeaderVisible(true);
		panel.setBodyBorder(false);
		panel.setCollapsible(false);  
		panel.setAnimCollapse(false);  
		panel.setButtonAlign(HorizontalAlignment.CENTER);  
		panel.setIconStyle("icon-table");
		panel.setLayout(new FitLayout());  
		panel.add(bankGrid);
		panel.setBottomComponent(toolBar);
		
		FormPanel inputForm = new FormPanel();  
		inputForm.setHeading("BLZ Input");
		
		blzInput = new TextField<String>();
		blzInput.setEmptyText("Please insert BLZ");
		blzInput.setFieldLabel("BLZ");
		blzInput.setAllowBlank(false);
		
		inputForm.add(blzInput);
		
		Button submitButton = new Button("Submit");
		submitButton.addListener(Events.Select, new Listener<BaseEvent>() {

			@Override
			public void handleEvent(BaseEvent be) {
				
				loader.load();
			}
		});
		
		inputForm.addButton(submitButton);

		RootPanel.get("blzInput").add(inputForm);
		RootPanel.get("bankTable").add(panel);
	}

	private class Bank extends BaseModel {

		public String getBezeichnung() {

			return get("bezeichnung");
		}

		public String getBIC() {

			return get("bic");
		}

		public String getOrt() {

			return get("ort");
		}

		public String getPLZ() {

			return get("plz");
		}
	}

	private class BankListDataReader implements DataReader<GWTWSPagingLoadConfig, PagingLoadResult<Bank>> {

		/**
		 * 
		 * @see com.extjs.gxt.ui.client.data.DataReader#read(java.lang.Object, java.lang.Object)
		 */
		public PagingLoadResult<Bank> read(GWTWSPagingLoadConfig loadConfig, Object data) {	
			List<Bank> entityList = new ArrayList<Bank>();

			int size = getEntitiesFromNode(((SoapMessage)data).getBody(), entityList);

			BasePagingLoadResult<Bank> result = new BasePagingLoadResult<Bank>(entityList, loadConfig.getOffset(), size);

			return result;
		}


		/**
		 * This method parses the XML-tree of a SOAP-response and creates a list of the desired target-beans.
		 * 
		 * @param node - the node containing the result-list
		 * @param listToAddTo - the list to which the beans should be added
		 * @return the size of the result-list (should be same as listToAddTo.size() after this method has been called)
		 */
		private int getEntitiesFromNode(Node node, List<Bank> listToAddTo) {

			return getEntitiesFromNode(node, listToAddTo, -1);
		}

		/**
		 * This method parses the XML-tree of a SOAP-response and creates a list of the desired target-beans.
		 * 
		 * This method should not be invoked directly, use getEntitiesFromNode(Node node, List<T> listToAddTo).
		 * 
		 * @param node - the node containing the result-list
		 * @param listToAddTo - the list to which the beans should be added
		 * @param size - the size of the result-list.
		 * @return
		 */
		private int getEntitiesFromNode(Node node, List<Bank> listToAddTo, int size) {

			if(node.getType() == Node.OBJECT_NODE) {

				if(node.getValue() instanceof List) {

					List<?> responseList = (List<?>)node.getValue();

					for(Object response : responseList) {

						if(response instanceof Entry) {
							Entry entry = (Entry)response;

							if(entry.getKey().endsWith("details")) {

								if(entry.getValue() instanceof List) {
									List<?> entityAttributeList = (List<?>)entry.getValue();

									Bank entity = new Bank();

									// Iterate through the attributes of this bean 
									for(Object attribute : entityAttributeList) {

										if(attribute instanceof Entry)
											entity.set(((Entry)attribute).getKey(), ((Entry)attribute).getValue());

									}

									// Add the created bean to the result-list
									listToAddTo.add(entity);
								}

							}
						}
					}
				}
			}

			if (node.getChilds() != null) {

				for(Object child : node.getChilds())
					size = getEntitiesFromNode((Node)child, listToAddTo, size);

			} else if(node.getValue() instanceof List) {

				for(Object subNode : (List<?>)node.getValue())
					if(subNode instanceof Node)
						size = getEntitiesFromNode((Node)subNode, listToAddTo, size);
			}

			if(size < listToAddTo.size())
				size = listToAddTo.size();

			return size;
		}

	}
}
