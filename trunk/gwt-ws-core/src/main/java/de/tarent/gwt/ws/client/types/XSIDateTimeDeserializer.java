/*
 * GWT WebService,
 * GWT WebService Implementation
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'GWT WebService'
 * Signature of Elmar Geese, 25 August 2008
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.gwt.ws.client.types;

import java.util.Date;

/**
 * 
 * A Deserializer for the "xsi:dateTime" type.
 * 
 * 
 * Should be compliant with the "XML Schema Part 2: Datatypes Second Edition,
 * W3C Recommendation 28 October 2004" [1] and ISO 8601 [2].
 * 
 * If it is not, please report a bug at [3] (assigning to Fabian K&ouml;ster) or contact the gwt-ws-devel Mailinglist [4].
 * 
 * TODO Due to the limitations of GWT 1.5 (Missing "real" Regular Expressions classes, Missing Calendar class),
 * this implementation is not only ugly but also error-prone and should therefore be re-implemented if better tools are available.
 * 
 * [1] http://www.w3.org/TR/2004/REC-xmlschema-2-20041028/datatypes.html#dateTime
 * [2] http://en.wikipedia.org/wiki/ISO_8601
 * [3] https://evolvis.org:443/tracker/?func=add&group_id=48&atid=243
 * [4] http://lists.evolvis.org/cgi-bin/mailman/listinfo/gwt-ws-devel
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class XSIDateTimeDeserializer {

	public static Date parseDateTime(String input) throws IllegalArgumentException {
		
		if(input == null)
			throw new IllegalArgumentException("input value must not be null");
		
		if(input == "")
			throw new IllegalArgumentException("input value must not be empty");
		
		/*
		 * According to the recommendation, the input must have the format
		 * 
		 * '-'? yyyy '-' mm '-' dd 'T' hh ':' mm ':' ss ('.' s+)? (zzzzzz)?
		 * 
		 * where zzzzzz is
		 * 
		 * ((('+'|'-')[0-9]{2}':'[0-9]{2})|'Z')?
		 *  
		 */
		
		if(!input.matches("-?[0-9]{4,}-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-6][0-9]:[0-6][0-9](.[0-9]+)?(((\\+|-)[0-9]{2}:[0-9]{2})|Z)?"))
			throw new IllegalArgumentException("Input \""+input+"\" is not valid");
		
		Date date = new Date();
		
		if(input.startsWith("-")) {
			
			// year is before common era
			// FIXME handle years before common era
			throw new UnsupportedOperationException("Years before common era are currently not supported. Input was: \""+input+"\"");
		}
		
		String dateString = input.substring(0, input.indexOf("T"));
		
		String[] datePortions = dateString.split("-");
		
		if(datePortions[0].length() > 4) {
			
			// FIXME handle years represented by more than 4 digits (cut of trailing zeros)
			throw new UnsupportedOperationException("More than 4 digits are currently not supported for years.");
		}
		
		int year = Integer.parseInt(datePortions[0]) - 1900;
		
		if(year == 0)
			throw new IllegalArgumentException("There is no year 0, \"0000\" is not a valid String for years. Input was: \""+input+"\"");
		
		date.setYear(year);
		
		int month = Integer.parseInt(datePortions[1]);
		
		if(month == 0  || month > 12)
			throw new IllegalArgumentException("Values for month must be between 1 and 12. Input was \""+input+"\"");

		date.setMonth(month-1);
		
		int day = Integer.parseInt(datePortions[2]);
		
		if(day == 0 || day > 31)
			throw new IllegalArgumentException("Values for day must be between 1 and 31. Input was \""+input+"\"");
		
		date.setDate(day);
		
		
		String timeString = input.substring(input.indexOf("T")+1);
		
		/* 
		 * If a timezone offset is specified, get the offset and apply it to the time later
		 */
		int timezoneMinutesOffset = 0;
		
		if(timeString.contains("+")) {
			
			String offsetString = timeString.substring(timeString.indexOf("+")+1);
			
			String[] offsetPortions = offsetString.split(":");
			
			timezoneMinutesOffset = - Integer.parseInt(offsetPortions[0]) * 60 +  Integer.parseInt(offsetPortions[1]);
			
		} else if(timeString.contains("-")) {
			
			String offsetString = timeString.substring(timeString.indexOf("-")+1);
			
			String[] offsetPortions = offsetString.split(":");
			
			timezoneMinutesOffset = Integer.parseInt(offsetPortions[0]) * 60 +  Integer.parseInt(offsetPortions[1]);
		}
			
		
		String[] timePortions = timeString.split(":");
		
		int hours = Integer.parseInt(timePortions[0]) + timezoneMinutesOffset / 60;
		
		if(hours < 0 || hours > 24)
			throw new IllegalArgumentException("Values for hours must be between 1 and 24. Input was \""+input+"\"");
		
		
		int minutes = Integer.parseInt(timePortions[1]) + timezoneMinutesOffset % 60;
		
		if(minutes < 0 || minutes > 60)
			throw new IllegalArgumentException("Values for minutes must be between 0 and 60. Input was \""+input+"\"");
		
		String secondsString = timePortions[2];
		
		int seconds;
		
		if(secondsString.contains("."))
			seconds = Integer.parseInt(secondsString.substring(0, secondsString.indexOf(".")));
		else if(secondsString.contains("+"))
			seconds = Integer.parseInt(secondsString.substring(0, secondsString.indexOf("+")));
		else if(secondsString.contains("-"))
				seconds = Integer.parseInt(secondsString.substring(0, secondsString.indexOf("-")));
		else if(secondsString.contains("Z"))
			seconds = Integer.parseInt(secondsString.substring(0, secondsString.indexOf("Z")));
		else
			seconds = Integer.parseInt(secondsString);
		
		if(seconds > 60)
			throw new IllegalArgumentException("Values for seconds must be between 0 and 60. Input was \""+input+"\"");
		
		return new Date(Date.UTC(year, month, day, hours, minutes, seconds));
	}
}
